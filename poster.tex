\documentclass[portrait,a0paper,fontscale=0.292]{baposter_mod}
\usepackage{tikz,xcolor,graphicx,tabularx,enumitem,fontspec,physics,amsmath,amssymb,braket}
\graphicspath{{images/}}
\setlist[itemize]{leftmargin=0.7em, itemsep=-0.1ex}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\setmainfont[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Bold,
  ItalicFont = SourceSansPro-It,
  BoldItalicFont = SourceSansPro-BoldIt
  ]{SourceSansPro-Regular}
\newfontfamily\light[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Semibold,
  ItalicFont = SourceSansPro-LightIt,
  BoldItalicFont = SourceSansPro-SemiboldIt
  ]{SourceSansPro-Light}
\newfontfamily\heavy[
  Path=fonts/,
  Ligatures=TeX,
  ItalicFont = SourceSansPro-BlackIt
  ]{SourceSansPro-Black}
% \setlength{\columnsep}{0.7em}
% \setlength{\columnseprule}{0mm}
% \setlength{\tabcolsep}{1mm}
% \hyphenpenalty=5000
\renewcommand\refname{}
\definecolor{colourbothnormal}{HTML}{5ec962}
% \definecolor{colourbase}{HTML}{5ec962}
\definecolor{colourupnormal}{HTML}{3a528b}
\definecolor{colourdownnormal}{HTML}{23938e}
\definecolor{coloursuperradiant}{HTML}{fee724}
\definecolor{colourunknown}{HTML}{440154}
% \definecolor{colourboxbase}{HTML}{108210}
\newcommand{\nodagger}{\phantom{$\dagger$}}
\newcommand{\colourone}{colourdownnormal}
\newcommand{\borderdarkness}{70}
\newcommand{\headerdarkness}{30}
\newcommand{\titleheight}{0.12}
\newcommand{\eyecatcherwidth}{8em}
\newcommand{\equationspacingmodifier}{\vspace{-2em}}

\begin{document}
    \begin{poster}
        {
          colspacing=3ex,
          columns=3,
          headerColorOne=\colourone!\headerdarkness!white,
          borderColor=\colourone!\borderdarkness!white,
          textborder=faded,
          headerborder=open,
          headershape=roundedright,
          headershade=plain,
          boxshade=none,
          boxColorOne = \colourone!20!white!90!black,
          background=none,
          headerheight=\titleheight\textheight
        }
        {
          \begin{minipage}{\eyecatcherwidth}
            Text.
          \end{minipage}          
        }
        {
          {\Huge \textbf{Simulating Continuous Symmetry Breaking} \\ 
          \vspace{0.4ex} \textbf{with Cold Atoms in Optical Cavities}}
        }
        {
          \vspace{1ex} {Ryan Moodie} \\
          {Supervisors: Jonathan Keeling and Kyle Ballantine} \\
          \vspace{1ex} {\large \textit{School of Physics and Astronomy, University of St Andrews}}
        }
        {
          \begin{minipage}{\eyecatcherwidth}
            \centering
            \includegraphics[height=0.09\textheight]{st_andrews_2}
          \end{minipage}
        }
        %%% Now define the boxes that make up the poster
        %%%---------------------------------------------------------------------------
        %%% Each box has a name and can be placed absolutely or relatively.
        %%% The only inconvenience is that you can only specify a relative position 
        %%% towards an already declared box. So if you have a box attached to the 
        %%% bottom, one to the top and a third one which should be inbetween, you 
        %%% have to specify the top and bottom boxes before you specify the middle 
        %%% box.
        \headerbox{Abstract}
        {name=_abstract,column=0,row=0,span=1}
        {
          \begin{itemize}
          \item Recent experiments with BEC in optical cavities have realised the Dicke phase transition \cite{Baumann2010}. 
          \item The resultant superradiant phase (SR) is associated with discrete $\mathbb{Z}_{2}$ symmetry breaking.
          \item We theoretically explore the rich physics of continuous $U(1)$ symmetry breaking, exhibited by an extended model.
          \item We aim to map out the complete phase diagram within mean-field theory.
          \end{itemize} 
        }
        \headerbox{Dicke model}
        {name=_original,column=0,below=_abstract,span=1}
        {
          \begin{itemize}
          \item The Dicke Hamiltonian describes an ensemble of two-level atoms interacting with a cavity mode ($\hbar = 1$):
          \begin{equation*}
            \hat{H} = \omega \hat{a}^{\dagger} \hat{a} 
            + \sum_{i} \bigg [ \omega_{0} \hat{s}^{z}_{i} 
            + g \Big ( \hat{a}^{\dagger} \hat{s}^{-}_{i} 
            + \hat{a}^{\dagger} \hat{s}^{+}_{i} + H.c. \Big ) \bigg ]
          \end{equation*}
          with $i^{th}$ atom as spin-$\frac{1}{2}$ particle:\\
          \begin{tabularx}{\textwidth}{l l}
            $\omega$ & mode energy \\
            $\hat{a}$, $\hat{a}^{\dagger}$ & mode annihilation/creation operator \\
            $\omega_{0}$ & atomic energy level splitting \\
            $\hat{s}_{i}^{z}$ & $z$-spin operator \\
            $g$ & light-matter coupling \\
            $\hat{s}_{i}^{-}$, $\hat{s}_{i}^{+}$ & spin lowering/raising operator \\
          \end{tabularx}\\\\
          \item Dicke phase transition predicted at critical $g$.
        \end{itemize}
        }
        \headerbox{Experiment description}
        {name=_experiment,column=0,below=_original,span=1}
        {
          \begin{itemize}
            \item Atoms (red) and light fields (blue) in a cavity with loss ($\kappa$) and pumping (from \cite{Bhaseen2012}):
          \end{itemize}
          \begin{center}
          \includegraphics[width=\textwidth]{chequerboard}\\
          \vspace{2ex}
          \begin{tabularx}{0.9\textwidth}{r|l l}
                  & Phase         & Cavity mode occupation  \\ \hline
            Left  & normal        & zero                    \\
            Right & SR  & macroscopic             \\
          \end{tabularx}
          \end{center} 
        }
        \headerbox{Effective model}
        {name=_effective,column=0,below=_experiment,span=1}
        {
          \begin{itemize}
            \item The Dicke phase transition can be realised in a nonequilibrium system.
            \item Effective two-level systems $\big ( \ket{0}$, $\ket{1} \big )$ are constructed using multilevel atoms and Raman channels (from \cite{Dimer2007}):
          \end{itemize}
          \begin{minipage}{0.65\textwidth}
            \includegraphics[width=\textwidth]{raman}\\
          \end{minipage}
          \hspace{-0.07\textwidth}
          \begin{minipage}{0.4\textwidth}
          \centering
          Transition\\
          mediation:\\
          \vspace{1ex}
            \begin{tabularx}{\textwidth}{c l}
              \begin{tikzpicture}
                \draw[red,thick] (0,0) -- (0.5,0);
              \end{tikzpicture} 
              & cavity mode \\
              \begin{tikzpicture}
                \draw[blue,dashed,thick] (0,0) -- (0.5,0);
              \end{tikzpicture} 
              & pump \\
            \end{tabularx}
          \end{minipage}
        }
        \headerbox{Two mode extension}{name=_twomode, column=1, span=1, row=0}
        {
        \begin{itemize}
          \item Extend the Dicke model by:
          \begin{itemize}
            \item adding second cavity mode labelled $b$
            \item adding feedback term, $U$, describing effects of spin state on mode energy and vice versa 
            \item making no assumptions about $g$
          \end{itemize}
          \item With collective spin and $\tilde{\omega}_{a,b} = \omega_{a,b} \pm U S^{z}$:
        \end{itemize}
        \vspace{-1em}
          \begin{multline*}
            \hat{H} = 
            \tilde{\omega}_{a} \hat{a}^{\dagger} \hat{a} 
            + \tilde{\omega}_{b} \hat{b}^{\dagger} \hat{b} + \omega_{0} \hat{S}^{z} 
            + \Big [
            g_{a} \hat{a}^{\dagger} \hat{S}^{-}  \\
            + g_{b} \hat{b}^{\dagger} \hat{S}^{+} 
            + g_{a}^{\prime} \hat{a}^{\dagger} \hat{S}^{+} 
            + g_{b}^{\prime} \hat{b}^{\dagger} \hat{S}^{-} + H.c. \Big ]
          \end{multline*}
        }
        \headerbox{Equations of motion}
        {name=_equations, column=1, span=1, below=_twomode}
        {
          \begin{itemize}
          \item Classical EOM (i.e. of operator expectation values) follow from the Lindblad equation:
          \begin{equation*}
            \dot{\hat{\rho}} = - i [\hat{H}, \hat{\rho}] 
            + \frac{\kappa_{a}}{2} \mathcal{L}[\hat{a}] 
            + \frac{\kappa_{b}}{2} \mathcal{L}[\hat{b}]
          \end{equation*}
          where
          \vspace{-1ex}
          \begin{equation*}
            \mathcal{L}[\hat{X}] = 2 \hat{X} \hat{\rho} \hat{X}^{\dagger} 
            - \hat{X}^{\dagger} \hat{X} \hat{\rho} 
            - \hat{\rho} \hat{X}^{\dagger} \hat{X} 
          \end{equation*}
          using
          \begin{equation*}
            \braket{\hat{X}} = Tr(\hat{\rho} \hat{X})
          \end{equation*}
          \item So, for example, with $\alpha = \braket{\hat{a}}$:
          \begin{equation*}
            \dot{\alpha} = - \left ( i \braket{[\hat{a}, \hat{H}]} 
            + \frac{\kappa_{a}}{2} \alpha \right )
          \end{equation*}
          \item Similarly,
          \begin{equation*}
            \beta = \braket{\hat{b}} \qquad 
            S^{+} = \braket{\hat{S}^{+}} \qquad 
            S^{z} = \braket{\hat{S}^{z}}
          \end{equation*}
          \item This yields (with semiclassical approximation):
          \begin{multline*}
            \dot{\alpha} = 
            - \bigg [ \Big ( i \tilde{\omega}_{a} 
            + \frac{\kappa_{a}}{2} \Big ) \alpha 
            + i \Big ( g_{a} S^{-} + g_{a}^{\prime} S^{+} \Big ) \bigg ]
          \end{multline*}
          \equationspacingmodifier
          \begin{multline*}
            \dot{\beta} = 
            - \bigg [ \Big ( i \tilde{\omega}_{b} 
            + \frac{\kappa_{b}}{2} \Big ) \beta 
            + i \Big ( g_{b} S^{+} + g_{b}^{\prime} S^{-} \Big ) \bigg ]
          \end{multline*}
          \equationspacingmodifier
          \begin{multline*}
            \dot{S^{+}} =
            i \, \bigg [ \omega_{0} S^{+} + U \Big ( \abs{\alpha}^{2} 
            + \abs{\beta}^{2} \Big ) S^{+} \\
            - 2 \Big ( g_{a} \alpha^{*} + {g_{a}^{\prime}}^{*} \alpha 
            + {g_{b}}^{*} \beta + g_{b}^{\prime} \beta^{*} \Big ) S^{z} \bigg ]
          \end{multline*}
          \equationspacingmodifier
          \begin{multline*}
            \dot{S^{z}} =
            i \, \bigg [ \Big ( g_a \alpha^{*} S^{-} - c.c \Big ) 
            + \Big ( g_a^{\prime} \alpha S^{-} - c.c. \Big ) \\
            + \Big ( g_b \beta S^{-} - c.c. \Big ) 
            + \Big ( g_b^{\prime} \beta^{*} S^{-} - c.c. \Big ) \bigg ]
            \end{multline*}
        \end{itemize}
        }
        \headerbox{Mapping the phase diagram}
        {name=_mapping, column=1, span=1, below=_equations}
        {
          \begin{itemize}
          \item Where simple attractors are expected, the steady state of the EOM is found. 
          \item Linear stability is checked using numerical diagonalisation of the matrix formed by the EOM of fluctuations.
          \item Otherwise, the EOM are time evolved by numerical integration to determine system behaviour.  
        \end{itemize}
        }
        % \headerbox{Continuous symmetry}
        % {name=_symmetry, column=2, span=1, row=0}
        % {
        %   \includegraphics[width=\textwidth]{compete}
        %   Order parameter plane (from \cite{Leonard2016}). The $d$-periodic interference potential moves continuously when varying the angle in the $\alpha$-$\beta$ plane. On transition, the system might assume any point on the circumference drawn, giving continuous $U(1)$ symmetry. If one mode is ``switched off'', we are confined to a single axis and so the state can assume only two points: this yields the discrete parity symmetry of the one-mode case.
        % }   
        \headerbox{Results}
        {name=_results, column=2, span=1, row=0}
        {
          \begin{itemize}
            \item With $g_{a} = g_{b} = g$, $g_{a}^{\prime} = g_{b}^{\prime} = g^{\prime}$ and $\kappa_{a} = \kappa_{b}$, we investigate solutions $\ket{\{s^z_i\},\alpha,\beta}$ for three cases.
              \item $g^{\prime} = g \, ; \;\; g, \, g^{\prime} \in \mathbb{R}$ 
              \begin{itemize}
                \item $\mathbb{Z}_{2}$ SR exists.
                \item For $U = 0$ and $\omega_{a} = \omega_{b} = \omega$:
              \end{itemize}
          \end{itemize}
          \begin{center}
            \includegraphics[width=\textwidth]{normal_omega_v_g}
          \end{center}
          \begin{itemize}
            \begin{itemize}
          \item For $U=0$ and $g=1$ kHz:
            \end{itemize}
          \end{itemize}
          \begin{center}
            \vspace{-1ex}
            \includegraphics[width=\textwidth]{normal_omega_a_v_omega_b} 
            \begin{tabularx}{\textwidth}{r l r l}
              \\
              \begin{tikzpicture}
                  \fill[colourdownnormal] circle (1ex);
                \end{tikzpicture} &
                 $\Downarrow \:\:\: = \ket{\downarrow\downarrow\downarrow...,0,0}$ &
              \begin{tikzpicture}
                  \fill[colourbothnormal] circle (1ex);
                \end{tikzpicture} &
                $\Uparrow$ \& $\Downarrow$    \\
              \begin{tikzpicture}
                  \fill[colourupnormal] circle (1ex);
                \end{tikzpicture} &
                $\Uparrow \:\:\: = \ket{\uparrow\uparrow\uparrow...,0,0}$ &
              \begin{tikzpicture}
                  \fill[colourunknown] circle (1ex);
                \end{tikzpicture} & 
                ?
              \\
              \begin{tikzpicture}
                  \fill[coloursuperradiant] circle (1ex);
                \end{tikzpicture} & 
                SR $ = \ket{\uparrow\downarrow\downarrow...,\alpha \neq 0,\beta \neq 0}$
              \\  
               &
                & \\ 
            \end{tabularx}
          \end{center}
            \begin{itemize}
              \item $g^{\prime} = 0 \,; \;\; g \in \mathbb{R}$ 
              \begin{itemize}
                \item For rotating solution of frequency $\mu$, \\
                      $U(1)$ SR exists for $\tilde{\omega}_{a} - \mu = \tilde{\omega}_{b} + \mu$.
              \end{itemize}
              \item $g^{\prime} = i g \,; \;\; g, \, g^{\prime} \in \mathbb{C}$
              \begin{itemize}
                \item Analytics suggest $U(1)$ SR exists.
              \end{itemize}
            \end{itemize}
        }
        \headerbox{Future outlook}
        {name=_curious, column=1, span=1, below=_mapping, above=bottom}
        {
          \begin{itemize}
            \item Does a line of $U(1)$ symmetry exist between the known points in the complex plane of $g^{\prime}$?
          \end{itemize}
        }
        \headerbox{Summary}
        {name=_summary, column=2, span=1, below=_results}
        {
          \begin{itemize}
            \item Dicke model extended to two cavity modes.
            \item This gives access to realisable regimes exhibiting continuous $U(1)$ symmetry.
            \item We are exploring the phases of the model associated with this symmetry.
          \end{itemize}
        }
        \headerbox{References}
        {name=_references, column=2, span=1, above=bottom, below=_summary}
        {
          \vspace{-1em}
          \footnotesize
          \raggedright
          \bibliographystyle{revtex_copy}
          \bibliography{references}
        }
    \end{poster}
\end{document}
