# Poster for two mode cavity Dicke model project #

Written in LaTeX. Uses the baposter template (https://github.com/mloesch/baposter) and Source Sans Pro font (https://github.com/adobe-fonts/source-sans-pro) (included in repo). 

### What is this repository for? ###

* Version control of the poster during development
* Easy sharing of the compiled pdf and LaTeX source code

### How do I get set up? ###

* Compile using XeLaTeX
* Pre-compiled pdf of poster included

### Contribution guidelines ###

* This repo is not open for contributions
* However, feel free to fork to use as a template!

### Who do I talk to? ###

* Repo owner
